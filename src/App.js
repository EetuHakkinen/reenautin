import React, { useState, useEffect } from 'react';
import './App.css';

const App = () => {
    const [value, setValue] = useState('');
    const [nbr1, setNbr1] = useState(0);
    const [nbr2, setNbr2] = useState(0);
    const [operator, setOperator] = useState('pow');
    const [text, setText] = useState('');
    const [show, setShow] = useState(false);
    const [time, setTime] = useState(0);
    const [stats, setStats] = useState([]);
    const [plus, setPlus] = useState(true);
    const [minus, setMinus] = useState(true);
    const [multiply, setMultiply] = useState(true);
    const [divide, setDivide] = useState(false);
    const [pow, setPow] = useState(true);
    const [count, setCount] = useState(0);

    useEffect(() => {
        generateNumbers();
    }, []);

    const generateNumbers = () => {
        setNbr1(Math.floor(Math.random() * 100));
        setNbr2(Math.floor(Math.random() * 100));
        generateOperator();
        setTime(Date.now());
    }

    const generateOperator = () => {
        var operatorList = [];
        if (plus) {
            operatorList.push('+');
        }
        if (minus) {
            operatorList.push('-');
        }
        if (multiply) {
            operatorList.push('*');
        }
        if (divide) {
            operatorList.push('/');
        }
        if (pow) {
            operatorList.push('^');
        }

        var random = Math.floor(Math.random() * operatorList.length);
        setOperator(operatorList[random]);
    }

    const checkAnswer = (e) => {
        e.preventDefault();
        setCount(count + 1);
        var t;
        if (text !== '') {
            setText('');
        }
        switch (operator) {
            case '+':
                if ((nbr1 + nbr2).toString() !== value) {
                    setText('wrong');
                } else {
                    t = Date.now() - time;
                    setText(t + ' ms');
                    if (!show) {
                        setStats(stats.concat(t));
                    }
                }
                break;
            case '-':
                if ((nbr1 - nbr2).toString() !== value) {
                    setText('wrong');
                } else {
                    t = Date.now() - time;
                    setText(t + ' ms');
                    if (!show) {
                        setStats(stats.concat(t));
                    }
                }
                break;
            case '*':
                if ((nbr1 * nbr2).toString() !== value) {
                    setText('wrong');
                } else {
                    t = Date.now() - time;
                    setText(t + ' ms');
                    if (!show) {
                        setStats(stats.concat(t));
                    }
                }
                break;
            case '/':
                if ((nbr1 / nbr2).toString() !== value) {
                    setText('wrong');
                } else {
                    t = Date.now() - time;
                    setText(t + ' ms');
                    if (!show) {
                        setStats(stats.concat(t));
                    }
                }
                break;
            case '^':
                if ((nbr1 * nbr1).toString() !== value) {
                    setText('wrong');
                } else {
                    t = Date.now() - time;
                    setText(t + ' ms');
                    if (!show) {
                        setStats(stats.concat(t));
                    }
                }
                break;
            default:
                break;
        }
        setValue('');
        setShow(false);
        generateNumbers();
    }

    return (
        <div>
            <h1>Reenautin</h1>
            {operator === '+' ? <div><span>{nbr1} + {nbr2}</span></div> : <div></div>}
            {operator === '-' ? <div><span>{nbr1} - {nbr2}</span></div> : <div></div>}
            {operator === '*' ? <div><span>{nbr1} * {nbr2}</span></div> : <div></div>}
            {operator === '/' ? <div><span>{nbr1} / {nbr2}</span></div> : <div></div>}
            {operator === '^' ? <div><span>{nbr1}</span><sup>2</sup></div> : <div></div>}
            <form onSubmit={checkAnswer}>
                <input
                    value={value}
                    onChange={e => setValue(e.target.value)} />
                <button type="submit">Tarkista</button>
            </form>
            <p>{text.toString()}</p>
            {show ? 
            <div></div>
            :<button onClick={() => setShow(true)}>show answer</button>}
            {show && operator === '+' ? <p>{nbr1 + nbr2}</p> : <div></div>}
            {show && operator === '-' ? <p>{nbr1 - nbr2}</p> : <div></div>}
            {show && operator === '*' ? <p>{nbr1 * nbr2}</p> : <div></div>}
            {show && operator === '/' ? <p>{nbr1 / nbr2}</p> : <div></div>}
            {show && operator === '^' ? <p>{nbr1 * nbr1}</p> : <div></div>}
            <h3>Settings:</h3>
            <form>
                <div>
                    <span>plus</span>
                    <input
                        type="checkbox"
                        checked={plus}
                        onChange={() => setPlus(!plus)}
                    />
                    <span>minus</span>
                    <input
                        type="checkbox"
                        checked={minus}
                        onChange={() => setMinus(!minus)}
                    />
                    <span>multiply</span>
                    <input
                        type="checkbox"
                        checked={multiply}
                        onChange={() => setMultiply(!multiply)}
                    />
                    <span>divide</span>
                    <input
                        type="checkbox"
                        checked={divide}
                        onChange={() => setDivide(!divide)}
                    />
                    <span>pow</span>
                    <input
                        type="checkbox"
                        checked={pow}
                        onChange={() => setPow(!pow)}
                    />
                </div>
            </form>
            <h3>Stats:</h3>
            <h5>Correct: {stats.length * 100 / count} %</h5>
            <div>
                <ul>
                    {stats.map((v, i) => <li key={i}>{v} ms</li>)}
                </ul>
            </div>
        </div>
    );
}

export default App;